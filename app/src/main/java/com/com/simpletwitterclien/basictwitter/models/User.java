package com.com.simpletwitterclien.basictwitter.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Anu on 6/29/14.
 */


public class User {

    private String name;
    private long userId;
    private String userScreenName;
    private String profileImageUrl;

    public static User fromJson(JSONObject jsonObjectUser)
    {
        User u = new User();
        try {
            u.name = jsonObjectUser.getString("name");
            u.userId = jsonObjectUser.getLong("id");
            u.userScreenName = jsonObjectUser.getString("screen_name");
            u.profileImageUrl = jsonObjectUser.getString("profile_image_url");

        }catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }
        return u;

    }

    public String getName() {
        return name;
    }

    public long getUserId() {
        return userId;
    }

    public String getUserScreenName() {
        return userScreenName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }
}
