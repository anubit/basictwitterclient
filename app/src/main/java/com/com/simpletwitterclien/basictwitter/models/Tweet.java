package com.com.simpletwitterclien.basictwitter.models;

import android.text.format.DateUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Anu on 6/29/14.
 */
// This class is to parse data json into java objects
// ans also you can use this class for formatting and validating the data
// determine which fields you want to extract from json
public class Tweet {

    private String tweetText;
    private static long tweetId; // made this as static to implement endless scroll
    private String tweetCreatedTime;
    private User tweetUser;

    // constructing a single tweet from JSON object
    // make sure in the get method, the string matches the one on the url.
    public static Tweet fromJsonObject(JSONObject jsonObject)
    {
        Tweet tweet = new Tweet();
        try {
            tweet.tweetText=jsonObject.getString("text");
            tweet.tweetId = jsonObject.getLong("id");
            tweet.tweetCreatedTime = jsonObject.getString("created_at");
            tweet.tweetUser = User.fromJson(jsonObject.getJSONObject("user"));


        }catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return tweet;
    }

    public String getTweetText() {
        return tweetText;
    }

    public static long getTweetId() {
        return tweetId;
    }

    public static long decrementedMaxId()
    {
        long max_id = Tweet.getTweetId() - 1;
        return  max_id;
    }

    public String getTweetCreatedTime() {
        return tweetCreatedTime;
    }

    public User getTweetUser() {
        return tweetUser;
    }

    // public static Collection<? extends Tweet> fromJSONArrayToTweetsArray(JSONArray jsonTweets) {
    // create an array list of tweets from an array of JSONObjects
    public static ArrayList<Tweet> fromJSONArrayToTweetsArray(JSONArray jsonArrayObject)
    {
        ArrayList<Tweet> tweetsArray = new ArrayList<Tweet>();
        // for each element in the big json array, extract the object and pass
        // that object to the static method fromJsonObject to extract a single tweet
        for(int i = 0; i < jsonArrayObject.length(); i++)
        {
            JSONObject singleJSONObject = null;
            try {
                singleJSONObject = jsonArrayObject.getJSONObject(i);
            } catch (Exception e)
            {
                e.printStackTrace();
                continue; // wny continue? and not return null?
            }

            // create a single tweet from the individual jsonobject
            Tweet tweet = Tweet.fromJsonObject(singleJSONObject);
            if (tweet != null)
            {
                tweetsArray.add(tweet);
            }
        }
        return tweetsArray;

    }

    // by layout of simple_list_item1 calls the toString method
    // and returns a list  as "com.android....at "(basically memory address)
    // Instead we want to display the tweet text, user, user screen name etc
    // this will give the list view as "the coolest clocks... (text) screenname"
    @Override
    public String toString() {
        return getTweetText() + " " + getTweetUser().getUserScreenName();
    }


    // getRelativeTimeAgo("Mon Apr 01 21:16:23 +0000 2014");
    // get the relative time for tweets
    // The method getRelativeTimeSpanString will display like 2 min ago, 2 weeeks ago et
    public static String getRelativeTimeAgo(String rawJsonDate) {
        String twitterFormat = "EEE MMM dd HH:mm:ss ZZZZZ yyyy";
        SimpleDateFormat sf = new SimpleDateFormat(twitterFormat, Locale.ENGLISH);
        sf.setLenient(true);

        String relativeDate = "";
        try {
            long dateMillis = sf.parse(rawJsonDate).getTime();
            relativeDate = DateUtils.getRelativeTimeSpanString(dateMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return relativeDate;
    }


}
