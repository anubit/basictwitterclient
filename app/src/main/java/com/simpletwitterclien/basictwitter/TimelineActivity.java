package com.simpletwitterclien.basictwitter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.com.simpletwitterclien.basictwitter.models.Tweet;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.simpletwitterclien.app.basictwitter.R;

import org.json.JSONArray;

import java.util.ArrayList;

// totalItemCount will give the number of count so far
// onscroll method should call loadmore to load more tweets
// I should call the api with this max id.
// getTweetid(count -1) - 1 is the max id


public class TimelineActivity extends Activity {

    private TwitterClient client;
    private ArrayList<Tweet> arrayOfTweets;
    private ArrayAdapter<Tweet> aTweetAdapter;
    private ListView lvTweets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        // we want to create an instance of twitter client created within the login activity
        // use that instance to send the authentication request
        // to get the json data
        // note: that getHomeTimeline method is defined inside twitterClientapp


        lvTweets = (ListView)findViewById(R.id.lvTweets);
        arrayOfTweets = new ArrayList<Tweet>();

        //aTweetAdapter = new ArrayAdapter<Tweet>(getBaseContext(),android.R.layout.simple_list_item_1,arrayOfTweets);

        //aTweetAdapter.clear(); --- when I clear the adapter here it does not work. why?
        aTweetAdapter = new TweetsArrayAdapter(this,arrayOfTweets);

        //client = TwitterClientApp.getRestClient();
        createHomeTimeline();
        lvTweets.setAdapter(aTweetAdapter);
        aTweetAdapter.clear();
    }



    // This method is to implement endless scroll feature
    private void loadMoreTweets(int totalItemsCount)
    {
        Toast.makeText(getBaseContext(),"totalItemsCount = " + totalItemsCount, Toast.LENGTH_SHORT).show();
        Toast.makeText(getBaseContext(),"load more tweets on scroll and call timeline",Toast.LENGTH_SHORT).show();

        aTweetAdapter.notifyDataSetChanged();

        createHomeTimelineWithEndlessScroll(totalItemsCount);
        aTweetAdapter.notifyDataSetChanged();
    }

    // This is to load tweets for endless scroll
    private void createHomeTimelineWithEndlessScroll(int totalItemsCount) {
        TwitterClientApp.getRestClient().getHomeTimelineWithEndlessScroll(new TwitterListViewHandler());
    }

    // This is to load tweets the first time
    private void createHomeTimeline() {
        TwitterClientApp.getRestClient().getHomeTimeline(new TwitterListViewHandler());
    }



    private class TwitterListViewHandler extends JsonHttpResponseHandler{

        // onsuccess we have to take the jSonArray (entire json object data)
        // and convert it to an array of tweets
        @Override
        public void onSuccess(JSONArray jsonArrayObject) {
            //ArrayList<Tweet> tweets = Tweet.fromJson(jsonTweets);
            arrayOfTweets.addAll(Tweet.fromJSONArrayToTweetsArray(jsonArrayObject));
            Log.d("debug", jsonArrayObject.toString());
            //adding the endless scroll feature
            lvTweets.setOnScrollListener(new EndlessScrollListener() {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {

                    Toast.makeText(getBaseContext(), "on load more in the client: totalItemsCount = " + totalItemsCount, Toast.LENGTH_SHORT).show();
                    Toast.makeText(getBaseContext(),"page = " + page, Toast.LENGTH_SHORT).show();
                    Toast.makeText(getBaseContext(),"call the load more data",Toast.LENGTH_SHORT).show();
                    //imageResults.clear();
                    loadMoreTweets(totalItemsCount);

                }
            });
        }
        @Override
        public void onFailure(Throwable e, String s) {
            Log.d("debug", e.toString());
            Log.d("debug", s);
        }
       /* @Override
        public void onSuccess(JSONArray json) {
            Log.d("debug", json.toString());

        }*/
    }
}

  /*  public void populateHomeTimeline()
    {
        // get the json data
        // this getHomeTimeline method has asynch handler to handle the data
        // we are getting the json data. it's better to use the json http async handler
        //
        //this onSuccess method takes json object or json array
        // if we look

        client.getHomeTimeline(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONArray json) {
                Log.d("debug", json.toString());

            }

            @Override
            public void onFailure(Throwable e, String s) {
                Log.d("debug", e.toString());
                Log.d("debug", s.toString());
            }

        });

    }
} */


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
 */

