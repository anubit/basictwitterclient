package com.simpletwitterclien.basictwitter;

/**
 * Created by Anu on 6/27/14.
 */

import android.content.Context;

import com.codepath.oauth.OAuthBaseClient;
import com.com.simpletwitterclien.basictwitter.models.Tweet;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.scribe.builder.api.Api;
import org.scribe.builder.api.TwitterApi;

/*
 *
 * This is the object responsible for communicating with a REST API.
 * Specify the constants below to change the API being communicated with.
 * See a full list of supported API classes:
 *   https://github.com/fernandezpablo85/scribe-java/tree/master/src/main/java/org/scribe/builder/api
 * Key and Secret are provided by the developer site for the given API i.e dev.twitter.com
 * Add methods for each relevant endpoint in the API.
 *
 * NOTE: You may want to rename this object based on the service i.e TwitterClient or FlickrClient
 *
 */
public class TwitterClient extends OAuthBaseClient {
    public static final Class<? extends Api> REST_API_CLASS = TwitterApi.class; // Change this
    public static final String REST_URL = "https://api.twitter.com/1.1"; // Change this, base API URL
    public static final String REST_CONSUMER_KEY = "Gn6I2gm73Pz9RPEYHhgHNGJrG";// Change this
    public static final String REST_CONSUMER_SECRET = "Dw5Jezk4GgAsUuxfsr7WcaUghda1kl05E6q2QhTM3PcvoxD4tU"; // Change this
    public static final String REST_CALLBACK_URL = "oauth://twitterapp"; // Change this (here and in manifest)

    public TwitterClient(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL);
       // Toast.makeText(getClient(),REST_CALLBACK_URL,Toast.LENGTH_SHORT).show();
    }

    // CHANGE THIS ---- for flicket
    // DEFINE METHODS for different API endpoints here
  /*  public void getInterestingnessList(AsyncHttpResponseHandler handler) {
        String apiUrl = getApiUrl("?nojsoncallback=1&method=flickr.interestingness.getList");
        // Can specify query string params directly or through RequestParams.
        RequestParams params = new RequestParams();
        params.put("format", "json");
        client.get(apiUrl, params, handler);
    } */

	/* 1. Define the endpoint URL with getApiUrl and pass a relative path to the endpoint
	 * 	  i.e getApiUrl("statuses/home_timeline.json");
	 * 2. Define the parameters to pass to the request (query or body)
	 *    i.e RequestParams params = new RequestParams("foo", "bar");
	 * 3. Define the request method and make a call to the client
	 *    i.e client.get(apiUrl, params, handler);
	 *    i.e client.post(apiUrl, params, handler);
	 */


    // for each endpoint we create a method
    // this method will get the json data
    // twitter loads 20 tweets at a time
    // if you have parameter to pass then you use RequestParams, otherwise
    // pass null and comment out the params

    public void getHomeTimeline(AsyncHttpResponseHandler handler)
    {
        String apiUrl = getApiUrl("statuses/home_timeline.json");
        RequestParams params = new RequestParams();
        params.put("since_id","1");
        client.get(apiUrl,params,handler);
        // for no params
        //client.get(apiUrl,null,handler);
    }

    // This endpoint will use max_id for endless scroll
    // max_id will display tweets less than its given id.
    // How to get max_id? you get the last tweet_id and decrement 1 to
    // return the ids less than (basically displays old tweets at the bottom)
    // set of tweets (default is 20), m

    public void getHomeTimelineWithEndlessScroll(AsyncHttpResponseHandler handler)
    {
        String apiUrl = getApiUrl("statuses/home_timeline.json");
        RequestParams params = new RequestParams();
       // if (LOADING_FIRST_TIME) {
            // to find the max_id from the given tweet
            long max_id = Tweet.decrementedMaxId();

            params.put("max_id",Long.toString(max_id));
            client.get(apiUrl, params, handler);
    }
}