package com.simpletwitterclien.basictwitter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.com.simpletwitterclien.basictwitter.models.Tweet;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.simpletwitterclien.app.basictwitter.R;

import java.util.List;

/**
 * Created by Anu on 6/29/14.
 */

// custom Array Adapter to display the arraylist of tweets.
// every custom Array adapter has a get view method
// In getView method, data item is translated into the list view.
// so far we have mentioned the layout as simple_list_item1, which is nothing but a textview,
// toString() method will just display in string

//basic array adapter will work for the listview of items, provided those items layout
// are the ones provided by android like simple_list_item1... (text view)
// In our list we want an image,a few textview etc.
// for that you need to create a custom array adapter
public class TweetsArrayAdapter extends ArrayAdapter<Tweet> {
    public TweetsArrayAdapter(Context context, List<Tweet> tweets) {
        super(context,0, tweets);
    }

    // this is the default getView
    // we haven't created our own layout yet.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // get the data item for position
        Tweet tweet = getItem(position);

        // find if the view exists. what that means if we reached the max limit
        // of items to view on the screen, then Android starts to recycle the
        // previous viewable list items.
        // If the view does not exist, we have to create one through the inflater
        View v;
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            v = inflater.inflate(R.layout.tweet_item,parent,false);
        }
        else {
            v = convertView;
        }

        // find the ids in the tweet_item.xml
        ImageView imvProfileImage = (ImageView)v.findViewById(R.id.imvProfileImage);
        TextView tvUserName = (TextView)v.findViewById(R.id.tvUserName);
        TextView tvScreenUserName = (TextView)v.findViewById(R.id.tvUserScreenName);
        TextView tvTweetText = (TextView)v.findViewById(R.id.tvTweetText);
        TextView tvCreatedAt = (TextView)v.findViewById(R.id.tvCreatedAt);

        ImageLoader imgLoader = ImageLoader.getInstance();

        // populate the items
        // use the image loader to load the image
        imgLoader.displayImage(tweet.getTweetUser().getProfileImageUrl(),imvProfileImage);
        tvUserName.setText(tweet.getTweetUser().getName());
        tvScreenUserName.setText(tweet.getTweetUser().getUserScreenName());
        tvTweetText.setText(tweet.getTweetText());
        tvCreatedAt.setText(Tweet.getRelativeTimeAgo(tweet.getTweetCreatedTime()));

       return v;

    }

}
